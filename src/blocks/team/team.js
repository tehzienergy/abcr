var teamSlider = function () {

  $('.team').each(function () {

    if ($(this).find('.teammate').length > 5 && $(window).width() >= 568) {

      $('.team').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 568,
            settings: {
              slidesToShow: 2
            }
          }
        ]
      })
    }

    else if ($(this).find('.teammate').length > 2 && $(window).width() < 568) {

      $('.team').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 568,
            settings: {
              slidesToShow: 2
            }
          }
        ]
      })
    }
  })
}

teamSlider();


$(window).on('resize', function() {
  teamSlider();
})
